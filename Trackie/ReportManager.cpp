#include "stdafx.h"
#include "ReportManager.h"


ReportManager::ReportManager(const ConfigManager& configManager)
{
	this->reportDirPath = configManager.getConfigValue("reportDirPath");
	this->emergencyReportDumpFile = configManager.getConfigValue("emergencyReportDumpFile");
	this->minProcessDuration = atoi(configManager.getConfigValue("minProcessDuration").c_str());

	this->accessMutex = CreateMutex(NULL, FALSE, L"RM_ACCESS_MUTEX");
}


ReportManager::~ReportManager()
{
	updateReports(false);

	WaitForSingleObject(this->accessMutex, INFINITE);

	std::list<TrackedProcess*>::iterator it;
	for (it = reportedProcesses.begin(); it != reportedProcesses.end(); it++)
	{
		delete *it;
	}
	
	CloseHandle(this->accessMutex);
}

void ReportManager::pushProcess(std::unique_ptr<TrackedProcess> processEntry)
{
	WaitForSingleObject(this->accessMutex, INFINITE);
	this->pendingProcesses.push_back(processEntry.release());
	ReleaseMutex(this->accessMutex);
}

void ReportManager::pushProcessContainer(std::unique_ptr<std::vector<std::unique_ptr<TrackedProcess>>> processVectorPtr)
{
	std::vector<std::unique_ptr<TrackedProcess>>* processVector = processVectorPtr.release();
	
	WaitForSingleObject(this->accessMutex, INFINITE);

	for (size_t i = 0; i < processVector->size(); i++)
	{
		this->pendingProcesses.push_back(processVector->at(i).release());
	}

	ReleaseMutex(this->accessMutex);

	delete processVector;
}

void ReportManager::updateReports(bool retryLaterIfFailed)
{
	std::ofstream reportFile;
	std::string prevAppName = "";
	std::string fullReportFilePath;

	std::list<TrackedProcess*> retryQueue;
	std::vector<std::pair<std::string, FILETIME>> modificationTimes;

	WaitForSingleObject(this->accessMutex, INFINITE);

	if (!pendingProcesses.empty())
	{
		pendingProcesses.sort(&processSortOrder);

		for (TrackedProcess* tP : pendingProcesses)
		{
			if (tP->getAppShortName().compare(prevAppName) != 0)
			{
				if (reportFile.is_open())
					reportFile.close();

				fullReportFilePath = this->reportDirPath + tP->getAppShortName() + std::string("_report.csv");
				
				if (!ReportManager::reportExists(fullReportFilePath))
					ReportManager::createReportFile(fullReportFilePath);
			

				//open report file
				try
				{
					reportFile.open(fullReportFilePath, std::ios_base::app);

					if (reportFile.fail())
						throw std::runtime_error("Unable to open report file");
				}
				catch (std::runtime_error exc)
				{
					if (!retryLaterIfFailed)
					{
						reportFile.open(this->emergencyReportDumpFile, std::ios_base::app);
						reportFile << tP->getAppShortName() << std::endl;
					}
				}

			}

			// write to report file (or push process to retry queue)
			if (reportFile.fail() && retryLaterIfFailed)
			{
				retryQueue.push_back(tP);
			}
			else
			{
				//create a list of modification times
				if (!retryLaterIfFailed)
				{
					try
					{
						modificationTimes.push_back(std::pair<std::string, FILETIME>(fullReportFilePath, this->getReportModificationTime(fullReportFilePath)));
					}
					catch (std::runtime_error exc)
					{
						// TODO: add handling
					}
				}
					
				addProcessToReport(reportFile, *tP);
				reportedProcesses.push_back(tP);
			}

			prevAppName = tP->getAppShortName();
		}
	}

	reportFile.close();

	pendingProcesses = retryQueue;
	ReleaseMutex(this->accessMutex);

	if (!retryLaterIfFailed)
	{
		haltUntilReportsModified(modificationTimes);
	}
}

bool ReportManager::processSortOrder(const TrackedProcess* lhs, const TrackedProcess* rhs)
{
	int strCmpResult = lhs->getAppShortName().compare(rhs->getAppShortName());

	if (strCmpResult < 0)
		return true;
	else if (strCmpResult == 0)
		return lhs->getStartTime() < rhs->getStartTime();
	else
		return false;
}

bool ReportManager::reportExists(const std::string& fullFilePath)
{
	FILE* file = fopen(fullFilePath.c_str(), "r");

	if (file != NULL)
	{
		fclose(file);
		return true;
	}

	return false;
}

void ReportManager::createReportFile(const std::string& fullFilePath)
{
	std::ofstream file(fullFilePath);

	if (!file.fail())
		file << "date;start;end;duration\n";
	else
		throw std::runtime_error("Unable to create report file");

	file.close();
}


void ReportManager::addProcessToReport(std::ofstream& reportFile, const TrackedProcess& proc)
{
	char buf[64];
	tm startTime, endTime;
	time_t startTimeT = proc.getStartTime();
	time_t endTimeT = proc.getEndTime();

	startTime = *localtime(&startTimeT);
	endTime = *localtime(&endTimeT);
	
	if (endTimeT - startTimeT > minProcessDuration)
	{
		if (reportFormat == ReportFileType::CSV)
		{
			//start date and time
			strftime(buf, 64, "%Y-%m-%d;%H:%M;", &startTime);
			reportFile << buf;

			//end time
			strftime(buf, 64, "%H:%M;", &endTime);
			reportFile << buf;

			//elapsed time
			endTimeT -= startTimeT;
			endTime = *gmtime(&endTimeT);
			sprintf(buf, "%02d:%02d\n", endTime.tm_hour, endTime.tm_min);
			reportFile << buf;
		}
		else
		{
			//start date and time
			strftime(buf, 64, "%Y-%m-%d %H:%M - ", &startTime);
			reportFile << buf;

			//end time
			strftime(buf, 64, "%H:%M ", &endTime);
			reportFile << buf;

			//elapsed time
			endTimeT -= startTimeT;
			endTime = *gmtime(&endTimeT);
			sprintf(buf, "(%02d:%02d)\n", endTime.tm_hour, endTime.tm_min);
			reportFile << buf;
		}
	}

}

//std::ofstream ReportManager::openReportFile(const std::string& appShortName)
//{
//
//}

FILETIME ReportManager::getReportModificationTime(const std::string& reportFilePath)
{
	std::wstring filepath(reportFilePath.begin(), reportFilePath.end());
	FILETIME modTime;
	HANDLE reportFile = CreateFile(filepath.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	int err = GetFileTime(reportFile, NULL, NULL, &modTime);
	CloseHandle(reportFile);

	if (err)
		throw std::runtime_error("Error while obtaining modification time");

	return modTime;
}


void ReportManager::haltUntilReportsModified(const std::vector<std::pair<std::string, FILETIME>>& reports)
{
	std::vector<bool> reported(reports.size());
	bool allReported = false;
	
	while (allReported == false)
	{
		allReported = true;
		for (int i = 0; i < reported.size(); i++)
		{
			if (reported[i] == false)
			{
				FILETIME time = this->getReportModificationTime(reports[i].first);
				if (CompareFileTime(&reports[i].second, &time) == -1)
					reported[i] = true;
				else
					allReported = false;
			}
		}

		if (allReported == false)
			Sleep(500);
	}
}


void ReportManager::printStatus() const
{
	this->printStatusToStream(std::cout);
}

std::string ReportManager::getStatus() const
{
	std::stringstream stream;

	this->printStatusToStream(stream);

	return std::string(stream.str());
}

void ReportManager::printStatusToStream(std::ostream& out) const
{
	out << std::endl << "Pending:" << std::endl;

	if (this->pendingProcesses.empty() == false)
	{
		for (const TrackedProcess* tP : pendingProcesses)
			out << *tP << std::endl;
	}
	else
		out << "none" << std::endl;

	if (this->reportedProcesses.empty() == false)
	{
		out << "Logged:" << std::endl;
		for (const TrackedProcess* tP : reportedProcesses)
			out << *tP << std::endl;
	}
}