#pragma once

#include <memory>
#include "AppDefinition.h"

class AppDefinitionBuilder
{
public:
	AppDefinitionBuilder();
	~AppDefinitionBuilder();

	void setLongName(const std::string& longName);
	void setShortName(const std::string& shortName);
	void setProcessName(const std::string& processName);
	void setExePath(const std::string& exePath);
	void setTracking(bool isTracked);

	std::unique_ptr<AppDefinition> getConstruct();

private:

	bool validateConstruct();

	AppDefinition* construct;
};

