#pragma once

#include "Tracker.h"
#include "ReportManager.h"

class DeadProcessConveyor
{
public:

	//receives dead processes from tracker and pushes them into ReportManager logging queue
	DeadProcessConveyor(Tracker& tracker, ReportManager& reportManager, unsigned int intervalSeconds);
	~DeadProcessConveyor();

	void start();
	void stop();

	bool isRunning() const;

private:

	void moveDeadProcesses();
	static DWORD WINAPI conveyorThread(void* args);

	bool stopSignal;
	bool running;
	int interval;

	HANDLE stopSignalMutex;
	HANDLE threadHandle;
	DWORD threadID;

	Tracker& tracker;
	ReportManager& reportManager;
};

