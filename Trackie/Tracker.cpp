#include "stdafx.h"
#include "Tracker.h"


Tracker::Tracker(std::unique_ptr<std::vector<AppDefinition>> trackedAppsList, unsigned int millisecInterval)
	: trackedApps(trackedAppsList.release()),
	snapshotInterval(millisecInterval),
	stopSignal(false),
	procSnapshotHandle(INVALID_HANDLE_VALUE)
{
	this->proc.dwSize = sizeof(PROCESSENTRY32);

	this->resourceAccessMutex = CreateMutex(NULL, false, NULL);
}

Tracker::Tracker(const ConfigManager& configManager)
	: trackedApps(configManager.getTrackedAppsList().release()),
	snapshotInterval(1000 * atoi(configManager.getConfigValue("trackerScanInterval").c_str())),
	stopSignal(false),
	procSnapshotHandle(INVALID_HANDLE_VALUE)
{
	this->proc.dwSize = sizeof(PROCESSENTRY32);

	this->resourceAccessMutex = CreateMutex(NULL, false, NULL);
}

Tracker::~Tracker()
{
	if (!this->stopSignal)
		this->stop();
	
	std::list<TrackedProcess*>::iterator it;
	for (it = trackedProcesses.begin(); it != trackedProcesses.end(); it++)
	{
		delete *it;
	}

	delete trackedApps;

	CloseHandle(resourceAccessMutex);
}

void Tracker::start()
{
	this->threadHandle = CreateThread(NULL, 0, &Tracker::trackerThread, (void*)this, NULL, &this->threadID);
	this->stopSignal = false;
}

void Tracker::stop()
{
	if (this->stopSignal == false)
	{
		this->stopSignal = true;

		WaitForSingleObject(this->threadHandle, INFINITE);

		CloseHandle(this->threadHandle);

		for (TrackedProcess* tp : trackedProcesses)
			tp->stopTracking();
	}
}

void Tracker::getProcessesSnapshot()
{
	this->procSnapshotHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
}

void Tracker::deleteProcessSnapshot()
{
	CloseHandle(this->procSnapshotHandle);
	this->procSnapshotHandle = INVALID_HANDLE_VALUE;
}

bool Tracker::addToTracking(const PROCESSENTRY32& process, const AppDefinition& matchingAppDefinition)
{
	bool processListed = false;

	for (TrackedProcess* tp : trackedProcesses)
	{
		if (tp->isAlive())
			if (tp->getPid() == process.th32ProcessID)
			{
				processListed = true;
				break;
			}
	}

	if (!processListed)
		trackedProcesses.push_back(new TrackedProcess(process, matchingAppDefinition));

	return !processListed;
}

void Tracker::updateTrackedStatus()
{
	for (TrackedProcess* tp : trackedProcesses)
	{
		if (tp->isAlive())
		{
			bool isAlive = false;

			Process32First(this->procSnapshotHandle, &this->proc);

			do
			{
				if (tp->getPid() == this->proc.th32ProcessID)
					if (tp->getProcessName().compare(proc.szExeFile) == 0)
						isAlive = true;

			} while (Process32Next(this->procSnapshotHandle, &this->proc));

			if (!isAlive)
				tp->stopTracking();
		}
	}
}

void Tracker::addUntrackedProcesses()
{
	//TODO iterate over processes in OUTER loop to allow tracking of multiple instances of the same app


	for (const AppDefinition& tA : *trackedApps)
	{
		Process32First(this->procSnapshotHandle, &this->proc);

		do
		{
			if (tA.getProcessNameW().compare(proc.szExeFile) == 0)
			{
				if (Tracker::isProcessInstanceOfApp(proc, tA))
				{
					this->addToTracking(this->proc, tA);
					break;
				}
			}

		} while (Process32Next(this->procSnapshotHandle, &this->proc));
	}

}

DWORD WINAPI Tracker::trackerThread(void* arg)
{
	Tracker* self = (Tracker*)arg;

	while (!self->stopSignal)
	{
		self->getProcessesSnapshot();

		WaitForSingleObject(self->resourceAccessMutex, INFINITE);

		self->updateTrackedStatus();
		self->addUntrackedProcesses();

		ReleaseMutex(self->resourceAccessMutex);

		self->deleteProcessSnapshot();

		Sleep(self->snapshotInterval);
	}

	return 0;
}

void Tracker::updateAppDefinitions(std::unique_ptr<std::vector<AppDefinition>> trackedAppsList)
{
	WaitForSingleObject(resourceAccessMutex, INFINITE);

	std::map<std::string, AppDefinition> definitionsInUse;
	std::vector<std::string> rejectedUpdates;

	for (TrackedProcess* tp : this->trackedProcesses)
		definitionsInUse.insert(std::pair<std::string, AppDefinition>(tp->getAppShortName(), tp->getAppDefinition()));

	// IMPORTANT! Deleting tracked apps results in trackees losing their AppDefs
	// Proposed workaround: leave trackedApps as it is, first remove all definitions that are not in use (no trackee uses them)
	//						then merge in the new def list (insert definitions only if they don't already exist)
	//delete this->trackedApps;
	//this->trackedApps = trackedAppsList.release();

	std::vector<AppDefinition>* newDefs = trackedAppsList.release();
	for (const AppDefinition& def : *newDefs)
	{
		// if definition is not currently used
		if (definitionsInUse.find(def.getShortName()) == definitionsInUse.end())
		{
			bool exists = false;
			for (AppDefinition& oldDef : *this->trackedApps)
			{
				if (oldDef.getShortName() == def.getShortName())
				{
					oldDef = def;
					exists = true;
					break;
				}
			}
			if (exists == false)
				this->trackedApps->push_back(def);
		}
	}

	//for (const std::pair<std::string, AppDefinition>& pair : definitionsInUse)
	//{
	//	bool exists = false;
	//	for (AppDefinition& appDef : *this->trackedApps)
	//	{
	//		if (appDef.getShortName() == pair.first)
	//		{
	//			appDef = pair.second;
	//			rejectedUpdates.push_back(pair.first);
	//			exists = true;
	//		}
	//	}
	//	if (exists == false)
	//		this->trackedApps->push_back(pair.second);
	//}

	delete newDefs;
	ReleaseMutex(resourceAccessMutex);

	//TODO return rejectedUpdates to notify user about applications, which definitons weren't updated
}

int Tracker::getTrackedCount(bool activeOnly) const
{
	int counter = 0;

	WaitForSingleObject(this->resourceAccessMutex, INFINITE);

	if (activeOnly)
		for (TrackedProcess* tp : trackedProcesses)
		{
			if (tp->isAlive())
				counter++;
		}
	else
		counter = trackedProcesses.size();

	ReleaseMutex(this->resourceAccessMutex);

	return counter;
}

int Tracker::getTerminatedCount() const
{
	int counter = 0;

	for (TrackedProcess* tp : trackedProcesses)
	{
		if (!tp->isAlive())
			counter++;
	}

	return counter;
}

std::unique_ptr<std::vector<TrackedProcess>> Tracker::getTrackedProcessesList(bool includeRunning) const
{
	std::vector<TrackedProcess>* ret = new std::vector<TrackedProcess>();

	WaitForSingleObject(this->resourceAccessMutex, INFINITE);

	if (includeRunning)
	{
		for (TrackedProcess* tp : trackedProcesses)
		{
			ret->emplace_back(tp);
		}
	}
	else
	{
		for (TrackedProcess* tp : trackedProcesses)
		{
			if (!tp->isAlive())
				ret->emplace_back(tp);
		}
	}

	ReleaseMutex(this->resourceAccessMutex);

	return std::unique_ptr<std::vector<TrackedProcess>>(ret);
}

std::unique_ptr<std::vector<std::unique_ptr<TrackedProcess>>> Tracker::yieldTerminatedProcesses()
{
	std::vector<std::unique_ptr<TrackedProcess>>* ret = new std::vector<std::unique_ptr<TrackedProcess>>();

	WaitForSingleObject(this->resourceAccessMutex, INFINITE);

	std::list<TrackedProcess*>::iterator it = trackedProcesses.begin();
	for (TrackedProcess* tp : trackedProcesses)
	{
		if (!tp->isAlive())
			ret->push_back(std::unique_ptr<TrackedProcess>(tp));
	}

	while (it != trackedProcesses.end())
	{
		if ((*it)->isAlive() == false)
		{
			it = trackedProcesses.erase(it);
		}
		else
			it++;
	}

	ReleaseMutex(this->resourceAccessMutex);

	return std::unique_ptr<std::vector<std::unique_ptr<TrackedProcess>>>(ret);
}

bool Tracker::isProcessInstanceOfApp(const PROCESSENTRY32& proc, const AppDefinition& app)
{
	std::wstring appFullExePath = app.getExePathW() + app.getProcessNameW();
	DWORD buflen = 350;
	wchar_t* buf = new wchar_t[buflen];

	HANDLE procH = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, proc.th32ProcessID);

	QueryFullProcessImageName(procH, NULL, buf, &buflen);

	bool isInstance = !appFullExePath.compare(buf);

	CloseHandle(procH);
	delete[] buf;

	return isInstance;
}

void Tracker::printStatus() const
{
	this->printStatusToStream(std::cout);
}

std::string Tracker::getStatus() const
{
	std::stringstream statusString;

	this->printStatusToStream(statusString);

	return std::string(statusString.str());
}

void Tracker::printStatusToStream(std::ostream& out) const
{
	out << std::endl << "Tracked: " << std::endl;

	if (this->trackedProcesses.empty() == false)
	{
		for (const TrackedProcess& tP : trackedProcesses)
			out << tP << std::endl;
	}
	else
		out << "none" << std::endl;
}

bool Tracker::isGameRunning() const
{
	bool gameTracked = false;

	WaitForSingleObject(this->resourceAccessMutex, INFINITE);

	for (TrackedProcess* tP : this->trackedProcesses)
		if (tP->isGame())
			gameTracked = true;

	ReleaseMutex(this->resourceAccessMutex);

	return gameTracked;
}