#pragma once

#include <Windows.h>
#include "ReportManager.h"
#include "Tracker.h"

//AutoReporter performs automatic report updates at interval specified in config file
//Updates are suspended if any game processes are running
class AutoReporter
{
public:
	AutoReporter(ReportManager& reportManager, const Tracker& tracker, unsigned int intervalMinutes);
	~AutoReporter();

	//stops automatic report updating
	//is userAction is true, userPause is enabled and automatic pausing/resuming (e.g. pause when a game is detected) won't have any effect
	void pause(bool userAction);

	//resumes AutoReporter operation
	//if userAction is true, userPause is disabled and automatic pausing/resuming is re-enabled
	void resume(bool userAction);

private:

	static DWORD WINAPI reporterThread(void* args);

	bool stopSignal;
	bool paused;
	bool userPause;
	unsigned int interval;

	ReportManager& reportManager;
	const Tracker& trackerHandle;

	HANDLE threadHandle;
	DWORD threadID;

	//this mutex is locked when reports are being updated
	HANDLE reportUpdateMutex;
};

