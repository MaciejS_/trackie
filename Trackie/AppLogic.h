#pragma once

#include "ConfigManager.h"
#include "ReportManager.h"
#include "Tracker.h"
#include "DeadProcessConveyor.h"
#include "AutoReporter.h"

class AppLogic
{
public:
	AppLogic(const std::string& appConfigPath);
	~AppLogic();

	//returns a string containing information about processes tracked by application during its lifetime
	std::string getApplicationStatus() const;

	//all 'pending' entries will be logged
	//if forceReport is true, pending entries that cannot be reported in their respective files will be saved to emergency dump file
	void performReportUpdate(bool forceReport);

	//stops the tracker and moves all tracked processes to logging queue
	void stopTracker();

	//disables automatic report updates
	void pauseAutoReporting();

	//re-enables automatic report updates
	void resumeAutoReporting();

	//drops current appDef database and loads new definitions
	//running trackers are automatically updated with new appDef list
	void reloadAppDefinitions();

private:
	ConfigManager config;
	Tracker tracker;
	ReportManager reportManager;
	DeadProcessConveyor conveyor;
	AutoReporter autoReporter;

};

