#include "stdafx.h"
#include "AppDefinition.h"

AppDefinition::AppDefinition()
	: longName(""),
	shortName(""),
	processName(""),
	exePath(""),
	processType(""),
	tracked(false),
	logRunning(false)
{

}

AppDefinition::~AppDefinition()
{
}

bool AppDefinition::isTracked() const { return this->tracked; }
bool AppDefinition::isLoggedWhileRunning() const { return this->logRunning; }
std::string AppDefinition::getLongName() const { return this->longName; }
std::string AppDefinition::getShortName() const { return this->shortName; }
std::string AppDefinition::getProcessName() const { return this->processName; }
std::string AppDefinition::getExePath() const { return this->exePath; }
std::string AppDefinition::getProcessType() const { return this->processType; }

std::wstring AppDefinition::getProcessNameW() const
{
	return std::wstring(processName.begin(), processName.end());
}

std::wstring AppDefinition::getExePathW() const
{
	return std::wstring(exePath.begin(), exePath.end());
}

std::istream& operator>>(std::istream& in, AppDefinition& def)
{
	std::string key, value, tag;
	char tmp[32];

	in >> key;


	if (sscanf(key.c_str(), "<%[^>]>", tmp) == 1)
	{
		tag = tmp;
		def.processType = tag;

		tag = "</";
		tag.append(tmp);
		tag.append(">");

		in >> key;

		do
		{
			in.ignore(3);
			std::getline(in, value);

			if (key == "longName")
				def.longName = value;
			else if (key == "shortName")
				def.shortName = value;
			else if (key == "processName")
				def.processName = value;
			else if (key == "exePath")
				def.exePath = value;
			else if (key == "tracked")
				def.tracked = (value == "true");
			else if (key == "logRunning")
				def.tracked = (value == "true");

			in >> key;

		} while (key.compare(tag) != 0);
	}
	else
	{
		//TODO handle invalid opening tag
	}

	return in;
}