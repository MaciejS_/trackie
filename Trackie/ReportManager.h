#pragma once

#include <string>
#include <memory>
#include <list>
#include <algorithm>
#include <fstream>
#include <sstream>
#include "TrackedProcess.h"
#include "ConfigManager.h"

enum ReportFileType
{
	CSV,
	TXT
};

class ReportManager
{
public:
	ReportManager(const ConfigManager& configManager);
	~ReportManager();

	//process entry will be added to appropriate app report
	void pushProcess(std::unique_ptr<TrackedProcess> processEntry);
	
	//push a collection of processes, contatiner is emptied in the process
	void pushProcessContainer(std::unique_ptr<std::vector<std::unique_ptr<TrackedProcess>>> processContainer);

	//updates report files with stored process entries
	//if argument is true, processes that couldn't be logged will remain in pending queue
	//if argument is false, such processes will be logged to emergency dump file
	void updateReports(bool retryLaterIfFailed);

	//performs a single write operation to report file
	void reportProcess(std::ofstream& reportFile, bool retryLaterIfFailed);

	//prints information about this object, processes pending and already logged to report files
	void printStatus() const;

	//returns a string contatining information about tracked process entries owned by this object
	std::string getStatus() const;

private:

	void addProcessToReport(std::ofstream& reportFile, const TrackedProcess& proc);
	static bool processSortOrder(const TrackedProcess* lhs, const TrackedProcess* rhs);

	//std::ofstream openReportFile(const std::string& appShortName);
	
	FILETIME ReportManager::getReportModificationTime(const std::string& reportFilePath);

	//this method polls file modification times of given reports until all had been modified
	//single pair in argument vector consists of full path to report file and last modification time for reference
	void haltUntilReportsModified(const std::vector<std::pair<std::string, FILETIME>>& reportModtimePairs);

	static bool reportExists(const std::string& fullReportFilePath);
	static void createReportFile(const std::string& fullReportFilePath);

	void printStatusToStream(std::ostream& out) const;

	std::list<TrackedProcess*> pendingProcesses;
	std::list<TrackedProcess*> reportedProcesses;

	std::string emergencyReportDumpFile;
	std::string reportDirPath;
	int minProcessDuration;
	ReportFileType reportFormat = ReportFileType::CSV;

	//this mutex is locked during access to process lists and report updates
	HANDLE accessMutex;
};

