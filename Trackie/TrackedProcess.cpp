#include "stdafx.h"
#include "TrackedProcess.h"


TrackedProcess::TrackedProcess(const PROCESSENTRY32& processPtr, const AppDefinition& relatedAppDef)
	: appDef(relatedAppDef),
	processID(processPtr.th32ProcessID),
	processName(processPtr.szExeFile),
	startTime(time(NULL)),
	endTime(NULL),
	running(true),
	uniqueID(TrackedProcess::getNewUID())
{
}

TrackedProcess::TrackedProcess(const TrackedProcess* other)
	: appDef(other->appDef),
	processID(other->processID),
	processName(other->processName),
	startTime(other->startTime),
	endTime(other->endTime),
	running(other->running),
	uniqueID(other->uniqueID)
{
}

TrackedProcess::TrackedProcess(const TrackedProcess& other)
	: appDef(other.appDef),
	processID(other.processID),
	processName(other.processName),
	startTime(other.startTime),
	endTime(other.endTime),
	running(other.running),
	uniqueID(other.uniqueID)
{
}

TrackedProcess::~TrackedProcess()
{
}
//
//TrackedProcess& TrackedProcess::operator= (const TrackedProcess& rhs)
//{
//	this->processID = rhs.processID;
//	this->processName = rhs.processName;
//	this->startTime = rhs.startTime;
//	this->endTime = rhs.endTime;
//	this->running = rhs.running;
//	this->appDef = rhs.appDef;
//
//	return *this;
//}

void TrackedProcess::stopTracking()
{
	if (running)
	{
		this->endTime = time(NULL);
		this->running = false;
	}
}

std::string TrackedProcess::toString() const
{
	std::string result(this->appDef.getShortName());

	char buf[32];
	tm* timeTM;

	//start time
	timeTM = localtime(&this->startTime);
	strftime(buf, 32, "\t%Y-%m-%d  %H:%M", timeTM);

	result.append(buf);

	if (!this->running)
	{
		//end time
		timeTM = localtime(&this->endTime);
		strftime(buf, 32, " - %H:%M ", timeTM);

		result.append(buf);

		//elapsed
		time_t elapsed = this->getProcessDuration();
		timeTM = gmtime(&elapsed);
		strftime(buf, 32, "(%H:%M)", timeTM);

		result.append(buf);
	}

	return result;
}

std::ostream& operator<< (std::ostream& out, const TrackedProcess& tp)
{
	out << tp.toString();

	return out;
}

time_t TrackedProcess::getStartTime() const
{
	return this->startTime;
}

time_t TrackedProcess::getEndTime() const
{
	return this->endTime;
}

time_t TrackedProcess::getProcessDuration() const
{
	return difftime(endTime, startTime);
}

std::string TrackedProcess::getAppShortName() const
{
	return this->appDef.getShortName();
}

std::wstring TrackedProcess::getProcessName() const
{
	return this->processName;
}

std::string TrackedProcess::getProcessType() const
{
	return this->appDef.getProcessName();
}

const AppDefinition& TrackedProcess::getAppDefinition() const
{
	return this->appDef;
}

int TrackedProcess::getPid() const
{
	return this->processID;
}

int TrackedProcess::getUniqueID() const
{
	return this->uniqueID;
}

bool TrackedProcess::isAlive() const
{
	return this->running;
}

bool TrackedProcess::isGame() const
{
	return this->appDef.getProcessType() == "game";
}

bool TrackedProcess::logWhileRunning() const
{
	return this->appDef.isLoggedWhileRunning();
}

int TrackedProcess::getNewUID()
{
	// currently, this method of 'generation' is sufficient
	static int uGen = 0;
	return uGen++;
}

bool TrackedProcess::operator== (const TrackedProcess& rhs) const
{
	return this->uniqueID == rhs.uniqueID;
}