#include "DeadProcessConveyor.h"


DeadProcessConveyor::DeadProcessConveyor(Tracker& processTracker, ReportManager& rM, unsigned int interval)
	: tracker(processTracker), 
	reportManager(rM), 
	running(false), 
	stopSignal(false), 
	interval(interval)
{
}


DeadProcessConveyor::~DeadProcessConveyor()
{
}

void DeadProcessConveyor::start()
{
	if (!this->running)
		this->threadHandle = CreateThread(NULL, NULL, &DeadProcessConveyor::conveyorThread, (void*)this, NULL, &this->threadID);
}

void DeadProcessConveyor::stop()
{
	if (this->running)
	{
		this->stopSignal = true;
		WaitForSingleObject(this->threadHandle, INFINITE);
	}
}

bool DeadProcessConveyor::isRunning() const
{
	return this->running;
}

DWORD WINAPI DeadProcessConveyor::conveyorThread(void* args)
{
	DeadProcessConveyor* self = (DeadProcessConveyor*)args;

	while (!self->stopSignal)
	{
		if (self->tracker.getTerminatedCount() > 0)
			self->moveDeadProcesses();

		Sleep(self->interval * 1000);
	}

	if (self->tracker.getTerminatedCount() > 0)
		self->moveDeadProcesses();

	self->stopSignal = false;
	return 0;
}

void DeadProcessConveyor::moveDeadProcesses()
{
	this->reportManager.pushProcessContainer(this->tracker.yieldTerminatedProcesses());
}