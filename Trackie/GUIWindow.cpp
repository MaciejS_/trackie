#include "GUIWindow.h"

GUIWindow::GUIWindow(HINSTANCE ownerHInstance) 
	: windowOwnerHInst(ownerHInstance)
{
}


GUIWindow::~GUIWindow()
{
}

HWND GUIWindow::getHWnd() const
{
	return this->hWnd;
}