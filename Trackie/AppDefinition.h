#pragma once
#include <string>
#include <istream>

class AppDefinition
{
	friend class AppDefinitionBuilder;
public:
	AppDefinition();
	/*AppDefinition(const std::string& longName, const std::string& processName);

	AppDefinition(const std::string& longName, const std::string& shortName, 
					const std::string& processName, bool isTracked);
*/
	~AppDefinition();

	std::string getLongName() const;
	std::string getShortName() const;
	std::string getProcessName() const;
	std::wstring getProcessNameW() const;
	std::string getExePath() const;
	std::wstring getExePathW() const;
	std::string getProcessType() const;

	bool isTracked() const;
	bool isLoggedWhileRunning() const;

	friend std::istream& operator>>(std::istream& in, AppDefinition& def);

private:
	std::string longName;
	std::string shortName;
	std::string processName;
	std::string exePath;
	std::string processType;

	bool tracked;
	bool logRunning;

};

