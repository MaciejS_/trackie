#include "stdafx.h"
#include "ConfigManager.h"


ConfigManager::ConfigManager(const std::string& configFilePath)
{
	try
	{
		loadConfig(configFilePath);
		loadDefinitions(this->config["appDefFilePath"]);
	}
	catch (int exc)
	{
		this->configLoaded = false;
		//TODO: user has to specify another path for the config file
		if (exc == 1)
			MessageBox(NULL, L"Fatal error: Unable to open config file!", L"Trackie", MB_ICONERROR | MB_OK);
		else if (exc == 2)
			MessageBox(NULL, L"Fatal error: App definitions not loaded!", L"Trackie", MB_ICONERROR | MB_OK);
	}
	
}


ConfigManager::~ConfigManager()
{
	/*std::map<std::string, AppDefinition*>::iterator it;

	/*for (it = this->appDefinitions.begin(); it != this->appDefinitions.end(); it++)
	{
		delete it->second;
		it->second = NULL;
	}*/
}


void ConfigManager::loadConfig(const std::string& configFilePath)
{
	std::ifstream file(configFilePath);
	std::pair<std::string, std::string> cfgEntry;

	if (file.is_open() && file.good())
	{
		while (!file.eof())
		{
			file >> cfgEntry.first;
			file.ignore(3);
			file >> cfgEntry.second;

			this->config.insert(cfgEntry);
		}
	}
	else
	{
		file.close();
		throw 1;
	}

	file.close();
}

void ConfigManager::loadDefinitions(const std::string& appDefFilePath)
{
	std::ifstream file(appDefFilePath);
	std::pair<std::string, AppDefinition> appEntry;

	if (file.is_open() && file.good())
	{
		while (!file.eof())
		{
			file >> appEntry.second;
			appEntry.first = appEntry.second.getShortName();

			this->appDefinitions.insert(appEntry);
		}
	}
	else
	{
		file.close();
		throw 2;
	}

	file.close();
}

void ConfigManager::updateDefinitions()
{
	this->appDefinitions.clear();
	loadDefinitions(this->config["appDefFilePath"]);
}

int ConfigManager::getAppsQty() const
{
	return this->appDefinitions.size();
}

int ConfigManager::getTrackedAppsQty() const
{
	int counter = 0;
	std::map<std::string, AppDefinition>::const_iterator it;

	for (it = this->appDefinitions.begin(); it != this->appDefinitions.end(); it++)
	{
		if (it->second.isTracked())
			counter++;
	}

	return counter;
}

std::unique_ptr<std::vector<AppDefinition>> ConfigManager::getTrackedAppsList() const
{
	std::vector<AppDefinition>* ret = new std::vector<AppDefinition>();

	std::map<std::string, AppDefinition>::const_iterator it;

	for (it = this->appDefinitions.begin(); it != this->appDefinitions.end(); it++)
	{
		if (it->second.isTracked())
			ret->emplace_back(it->second);
	}

	return std::unique_ptr<std::vector<AppDefinition>>(ret);
}

const std::string& ConfigManager::getConfigValue(const std::string& configEntryName) const
{
	try
	{
		return this->config.at(configEntryName);
	}
	catch (std::out_of_range exception)
	{
		throw std::runtime_error(configEntryName + " not found in config file\n");
	}
}