#pragma once
#include <ctime>
#include <string>
#include <Windows.h>
#include <TlHelp32.h>
#include "AppDefinition.h"

class TrackedProcess
{
public:
	TrackedProcess(const PROCESSENTRY32& processPtr, const AppDefinition& relatedAppDef);
	TrackedProcess(const TrackedProcess* other);
	TrackedProcess(const TrackedProcess& other);
	~TrackedProcess();

	//marks the process as stopped a
	void stopTracking();

	time_t getStartTime() const;
	time_t getEndTime() const;
	time_t getProcessDuration() const;

	bool isAlive() const;
	bool isGame() const;
	bool logWhileRunning() const;

	std::wstring getProcessName() const;
	std::string getAppShortName() const;
	std::string getProcessType() const;

	std::string toString() const;

	//Returns system-given ProcessID associated with this object
	int getPid() const;

	//Returns uniqueID of this object
	int getUniqueID() const;

	//returns AppDefinition related to this TrackedProcess
	const AppDefinition& getAppDefinition() const;

	//TrackedProcess& operator= (const TrackedProcess& rhs);
	bool operator== (const TrackedProcess& rhs) const;

	friend std::ostream& operator<< (std::ostream& out, const TrackedProcess& rhs);



private:

	//generates a uniqueID value
	static int getNewUID();

	time_t startTime;
	time_t endTime;
	std::wstring processName;
	const AppDefinition& appDef;
	int processID;
	int uniqueID;	// since Windows is reusing processIDs, uniqueID is needed

	bool running;
};

