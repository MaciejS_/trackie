#include "AppLogic.h"


AppLogic::AppLogic(const std::string& appConfigPath) : 
	config(appConfigPath), 
	tracker(config),
	reportManager(config),
	conveyor(tracker, reportManager, atoi(config.getConfigValue("conveyorIntervalSeconds").c_str())),
	autoReporter(reportManager, tracker, atoi(config.getConfigValue("autoReportIntervalMinutes").c_str()))
{
	tracker.start();
	conveyor.start();
}


AppLogic::~AppLogic()
{
	tracker.stop();
	conveyor.stop();
}

std::string AppLogic::getApplicationStatus() const
{
	std::stringstream stream;

	stream << this->tracker.getStatus() << this->reportManager.getStatus();

	return std::string(stream.str());
}

void AppLogic::performReportUpdate(bool forceReport)
{
	//see updateReports declaration if !forceReport appears confusing
	this->reportManager.updateReports(!forceReport);
}

void AppLogic::stopTracker()
{
	this->tracker.stop();
	this->conveyor.stop();

	if (this->tracker.getTerminatedCount() > 0)
		this->reportManager.pushProcessContainer(this->tracker.yieldTerminatedProcesses());
}

void AppLogic::pauseAutoReporting()
{
	autoReporter.pause(true);
}

void AppLogic::resumeAutoReporting()
{
	autoReporter.resume(true);
}

void AppLogic::reloadAppDefinitions()
{
	config.updateDefinitions();
	tracker.updateAppDefinitions(config.getTrackedAppsList());
}