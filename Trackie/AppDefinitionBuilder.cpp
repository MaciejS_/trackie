#include "stdafx.h"
#include "AppDefinitionBuilder.h"


AppDefinitionBuilder::AppDefinitionBuilder()
{
	this->construct = new AppDefinition();
}


AppDefinitionBuilder::~AppDefinitionBuilder()
{
	delete construct;
}

void AppDefinitionBuilder::setLongName(const std::string& longName)
{
	this->construct->longName = longName;
}

void AppDefinitionBuilder::setShortName(const std::string& shortName)
{
	this->construct->shortName = shortName;
}

void AppDefinitionBuilder::setProcessName(const std::string& processName)
{
	this->construct->processName = processName;
}

void AppDefinitionBuilder::setExePath(const std::string& exePath)
{
	this->construct->exePath = exePath;
}

void AppDefinitionBuilder::setTracking(bool isTracked)
{
	this->construct->tracked = isTracked;
}

bool AppDefinitionBuilder::validateConstruct()
{
	if (this->construct->longName.length() == 0)
		return false;
	else if (this->construct->shortName.length() == 0)
		return false;
	else if (this->construct->processName.length() == 0)
		return false;
	else if (this->construct->exePath.length() == 0)
		return false;
	else
		return true;
}

std::unique_ptr<AppDefinition> AppDefinitionBuilder::getConstruct()
{
	if (this->validateConstruct())
	{
		std::unique_ptr<AppDefinition> ret(this->construct);
		this->construct = new AppDefinition();
		return ret;
	}
	else
		throw std::runtime_error("Incomplete construct");
}