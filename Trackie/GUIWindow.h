#pragma once

#include "Resource.h"
#include <Windows.h>
#include <stdexcept>

/*
	Classes inheriting from GUIWindow must implement wndProc on their own
*/

class GUIWindow
{
public:
	GUIWindow(HINSTANCE ownerHInstance);
	~GUIWindow();

	HWND getHWnd() const;

protected:

	virtual void registerWindowClass() = 0;

	WNDCLASSEX windowClass;
	HWND hWnd;
	HWND hButton;
	HMENU hMenu;

	HINSTANCE windowOwnerHInst;
};

