#include "AutoReporter.h"

AutoReporter::AutoReporter(ReportManager& reportManager, const Tracker& tracker, unsigned int intervalMinutes) 
	: reportManager(reportManager),
	trackerHandle(tracker),
	interval(intervalMinutes),
	paused(false),
	userPause(false),
	stopSignal(false)
{
	this->threadHandle = CreateThread(NULL, NULL, AutoReporter::reporterThread, this, NULL, &threadID);
	this->reportUpdateMutex = CreateMutex(NULL, FALSE, L"AR_REPORTINGNOW_MUTEX");
}

AutoReporter::~AutoReporter()
{
	this->stopSignal = true;

	WaitForSingleObject(this->reportUpdateMutex, INFINITE);

	if (WaitForSingleObject(this->threadHandle, 1000) == WAIT_TIMEOUT)
		TerminateThread(this->threadHandle, -1);

	CloseHandle(this->reportUpdateMutex);
	CloseHandle(this->threadHandle);
}

void AutoReporter::pause(bool userAction)
{
	if (userAction && !this->userPause)
		this->userPause = true;
	else if (!userAction && !this->paused)
		this->paused = true;
}

void AutoReporter::resume(bool userAction)
{
	if (userAction && this->userPause)
		this->userPause = false;
	else if (!userAction && this->paused)
		this->paused = false;
}

DWORD WINAPI AutoReporter::reporterThread(void* args)
{
	AutoReporter* self = (AutoReporter*)args;

	while (self->stopSignal == false)
	{
		if (self->userPause == false)
		{
			if (self->trackerHandle.isGameRunning())
				self->pause(false);
			else
				self->resume(false);

			if (!self->paused && !self->userPause)
			{
				WaitForSingleObject(self->reportUpdateMutex, INFINITE);
				self->reportManager.updateReports(true);
				ReleaseMutex(self->reportUpdateMutex);
			}
		}

		Sleep(self->interval * 60 * 1000);
	}

	return 0;
}