#pragma once

#include <sstream>
#include <vector>
#include <memory>
#include <list>
#include "TrackedProcess.h"
#include "AppDefinition.h"
#include "ConfigManager.h"

class Tracker
{
public:
	Tracker(std::unique_ptr<std::vector<AppDefinition>> trackedAppsList, unsigned int millisecInterval);

	//object will automatically construct itself using information stored within configManager
	Tracker(const ConfigManager& configManager);

	~Tracker();

	//starts the tracker
	void start();

	//stops the tracker
	void stop();

	//returns number of tracked processes
	//if active is false, dead processes are also counted
	int getTrackedCount(bool activeOnly) const;

	//returns number of tracked processes that have been terminated
	int getTerminatedCount() const;

	//returns true if tracker is currently tracking at least one game process
	bool isGameRunning() const;

	//swaps applications definitions
	void updateAppDefinitions(std::unique_ptr<std::vector<AppDefinition>> trackedAppsList);

	//returns a vector of tracked processes
	//if includeRunning is false, only dead processes are returned
	std::unique_ptr<std::vector<TrackedProcess>> getTrackedProcessesList(bool includeRunning) const;

	//removes returned processes from tracker's internal process list
	std::unique_ptr<std::vector<std::unique_ptr<TrackedProcess>>> yieldTerminatedProcesses();

	//prints information about currently tracked processes
	void printStatus() const;

	//returns a string containing information about currently tracked processes
	std::string getStatus() const;

private:

	static DWORD WINAPI trackerThread(void* arg);

	void getProcessesSnapshot();
	void deleteProcessSnapshot();

	//checks running processes snapshot and updates tracked processes list accordingly
	void updateTrackedStatus();

	//checks running process list and adds new processes of tracked apps 
	void addUntrackedProcesses();

	//adds process to tracked list, performs a check to ensure that process is not tracked twice
	//returns false if process is already being tracked, otherwise, true is returned
	bool addToTracking(const PROCESSENTRY32& process, const AppDefinition& matchingAppDefinition);

	static bool isProcessInstanceOfApp(const PROCESSENTRY32& process, const AppDefinition& app);


	void printStatusToStream(std::ostream& out) const;

	std::list<TrackedProcess*> trackedProcesses;
	std::vector<AppDefinition>* trackedApps;

	int snapshotInterval;
	bool stopSignal;

	HANDLE procSnapshotHandle;
	PROCESSENTRY32 proc;

	HANDLE threadHandle;
	DWORD threadID;

	//provides access to trackedProcesses list and trackedApps vector
	HANDLE resourceAccessMutex;
};

