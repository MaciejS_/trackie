// Trackie.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Trackie.h"
#include <Windows.h>
#include <shellapi.h>
#include <iostream>

#include "MainWindow.h"


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	MainWindow guiWnd(hInstance);

	MSG msg;
	HACCEL hAccelTable;


	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TRACKIE));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

}