#pragma once
#include "GUIWindow.h"

#include "AppLogic.h"

class MainWindow :
	public GUIWindow
{
public:
	MainWindow(HINSTANCE ownerHInstance);
	~MainWindow();

private:
	static LRESULT CALLBACK wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	virtual void registerWindowClass();
	virtual void addTrayIcon();
	virtual void removeTrayIcon();

	static const int MSG_TRAYICON = 0x8001;
	static const int ID_TRAY = 601;
	static const int SWIG_PASS_SELF_PTR = WM_USER + 2;

private:
	void prepareTrayIcon(HWND hWnd, UINT iconFlags);

	NOTIFYICONDATA notifyIconData;

	//singleton AppLogic object
	static AppLogic appLogic;

};

