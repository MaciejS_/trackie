#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <memory>
#include <vector>
#include "AppDefinition.h"

class ConfigManager
{
public:
	ConfigManager(const std::string& configFilePath);
	~ConfigManager();

	int getAppsQty() const;
	int getTrackedAppsQty() const;

	//returns config value corresponding to configEntryName key
	//if key cannot be found std::runtime_error is thrown
	const std::string& getConfigValue(const std::string& configEntryName) const;

	std::unique_ptr<std::vector<AppDefinition>> getTrackedAppsList() const;

	void updateDefinitions();

private:

	void loadConfig(const std::string& configFilePath);
	void loadDefinitions(const std::string& definitionFilePath);

	std::map<std::string, std::string> config;
	std::map<std::string, AppDefinition> appDefinitions;

	bool configLoaded;
};

