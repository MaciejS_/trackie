#include "MainWindow.h"

//singleton AppLogic object
AppLogic MainWindow::appLogic = AppLogic("./cfg/config.cfg");

MainWindow::MainWindow(HINSTANCE ownerHInstance) : GUIWindow(ownerHInstance)
{
	this->registerWindowClass();

	this->hWnd = CreateWindow(L"MainWindow", L"Trackie", WS_OVERLAPPEDWINDOW,
		0, 0, 0, 0, NULL, NULL, ownerHInstance, NULL);

	this->prepareTrayIcon(this->hWnd, NIF_MESSAGE | NIF_ICON | NIF_TIP);

	SendMessage(this->hWnd, MainWindow::SWIG_PASS_SELF_PTR, NULL, (LPARAM)this);

	this->addTrayIcon();

	ShowWindow(this->hWnd, SW_HIDE);
	UpdateWindow(this->hWnd);
}


MainWindow::~MainWindow()
{
	this->appLogic.stopTracker();
	this->appLogic.performReportUpdate(true);
	this->removeTrayIcon();
}

LRESULT CALLBACK MainWindow::wndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static HMENU hMenu, hPopupMenu;
	static UINT taskbarCreateMsg;
	static MainWindow* self;


	if (msg == taskbarCreateMsg)
	{
		self->addTrayIcon();
	}

	switch (msg)
	{
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_STATUS:
			MessageBoxA(NULL, MainWindow::appLogic.getApplicationStatus().c_str(), "Trackie Status", MB_OK);
			break;
		case IDM_UPDATE:
			MainWindow::appLogic.performReportUpdate(false);
			break;
		case IDM_RELOAD_APPDEFS:
			MainWindow::appLogic.reloadAppDefinitions();
			break;
		case IDM_ABOUT:
			//DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, msg, wParam, lParam);
			break;
		}
		break;
	case MSG_TRAYICON:
		if (lParam == WM_RBUTTONDOWN)
		{
			hMenu = LoadMenu(GetModuleHandle(NULL), MAKEINTRESOURCE(IDC_TRACKIE));
			hPopupMenu = GetSubMenu(hMenu, 0);

			SetForegroundWindow(hWnd);

			POINT p;
			GetCursorPos(&p);

			TrackPopupMenu(hPopupMenu, TPM_LEFTALIGN | TPM_BOTTOMALIGN | TPM_LEFTBUTTON | TPM_NOANIMATION, p.x, p.y, 0, hWnd, 0);

			DestroyMenu(hMenu);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_CREATE:
		taskbarCreateMsg = RegisterWindowMessageA("TaskbarCreated");
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_QUERYENDSESSION:
		// prevents Windows from terminating the application before it performs necessary cleanup
		if (!ShutdownBlockReasonCreate(hWnd, L"Updating reports..."))
			MessageBox(hWnd, L"BlockReasonFailed!", L"Trackie", MB_OK | MB_ICONERROR);
		return TRUE;
	case WM_ENDSESSION:
		MainWindow::appLogic.stopTracker();
		MainWindow::appLogic.performReportUpdate(true);
		ShutdownBlockReasonDestroy(hWnd);
		break;
	case SWIG_PASS_SELF_PTR:
		self = (MainWindow*)lParam;
	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
	return 0;

}

void MainWindow::registerWindowClass()
{
	this->windowClass.cbSize = sizeof(WNDCLASSEX);
	this->windowClass.style = 0;
	this->windowClass.lpfnWndProc = (WNDPROC)&MainWindow::wndProc;
	this->windowClass.cbClsExtra = 0;
	this->windowClass.cbWndExtra = 0;
	this->windowClass.hInstance = GUIWindow::windowOwnerHInst;
	this->windowClass.hIcon = LoadIcon(GUIWindow::windowOwnerHInst, MAKEINTRESOURCE(IDI_TRACKIE));
	this->windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	this->windowClass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	this->windowClass.lpszMenuName = MAKEINTRESOURCE(IDC_TRACKIE);
	this->windowClass.lpszClassName = L"MainWindow";
	this->windowClass.hIconSm = LoadIcon(windowClass.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	if (!RegisterClassEx(&this->windowClass))
		throw std::runtime_error("Unable to register window class");
}

void MainWindow::addTrayIcon()
{
	Shell_NotifyIcon(NIM_ADD, &this->notifyIconData);
}

void MainWindow::removeTrayIcon()
{
	Shell_NotifyIcon(NIM_DELETE, &this->notifyIconData);
}

void MainWindow::prepareTrayIcon(HWND hWnd, UINT iconFlags)
{
	memset(&this->notifyIconData, 0, sizeof(NOTIFYICONDATA));

	this->notifyIconData.cbSize = sizeof(NOTIFYICONDATA);
	this->notifyIconData.hIcon = LoadIcon(GUIWindow::windowOwnerHInst, MAKEINTRESOURCE(IDI_SMALL));
	this->notifyIconData.hWnd = hWnd;
	this->notifyIconData.uID = MainWindow::ID_TRAY;
	this->notifyIconData.uCallbackMessage = MainWindow::MSG_TRAYICON;
	this->notifyIconData.uFlags = iconFlags;
	lstrcpy(this->notifyIconData.szTip, L"Trackie");
}